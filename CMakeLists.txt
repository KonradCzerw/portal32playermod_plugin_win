cmake_minimum_required(VERSION 3.0.0)
project(portal32playermod_plugin VERSION 0.1.0)

set(TOOLCHAIN_PREFIX i686-w64-mingw32)

set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PREFIX}-g++)
set(CMAKE_FIND_ROOT_PATH /usr/${TOOLCHAIN_PREFIX})

FILE(GLOB cppSrc *.cpp)
add_library(32pmod SHARED ${cppSrc})
target_link_libraries(32pmod psapi)
add_custom_command(TARGET 32pmod POST_BUILD
	COMMAND ${TOOLCHAIN_PREFIX}-strip $<TARGET_FILE:32pmod>
	COMMENT "Stripped $<TARGET_FILE:32pmod>"
	COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:32pmod> ${CMAKE_SOURCE_DIR}/32pmod.dll
	COMMENT "Copied binary to ${CMAKE_SOURCE_DIR}/32pmod.dll"
)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m32 -g -Wall -Werror -O0")
SET(CMAKE_SHARED_LINKER_FLAGS "-m32 -static")

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)

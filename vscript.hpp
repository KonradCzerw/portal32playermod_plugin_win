#ifndef VSCRIPT_HPP
#define VSCRIPT_HPP

#include "sdk.hpp"
#include "interface.hpp"
#include "utils.hpp"

class VScript {
public:
	Interface* scriptmanager;

	using _LookupFunction = HSCRIPT(__thiscall*)(void* thisptr, const char* pszFunction, HSCRIPT hScope);
	using _ExecuteFunction = ScriptStatus_t(__thiscall*)(void* thisptr, HSCRIPT hFunction, ScriptVariant_t* pArgs, int nArgs, ScriptVariant_t* pReturn, HSCRIPT hScope, bool bWait);

	_LookupFunction LookupFunction = nullptr;
	_ExecuteFunction ExecuteFunction = nullptr;

public:
	VScript();
	virtual ~VScript() = default;

	bool Init();
	void Shutdown();
	const char* Name() { return MODULE("vscript"); }

	DECL_DETOUR_STD(IScriptVM*, CreateVM, ScriptLanguage_t language);

public:
	void DoChatCallbacks(int id, char* message);
	void ResetVM();

	template<typename... Args> ScriptVariant_t Call(const char* pszFunction, Args... args) {
		if(!this->g_pScriptVM || !this->LookupFunction || !this->ExecuteFunction)
			return SCRIPT_ERROR;
		ScriptVariant_t aArgs[] = { args... };
		HSCRIPT hFunction = this->LookupFunction(this->g_pScriptVM, pszFunction, nullptr);
		if(!hFunction) return SCRIPT_ERROR;
		return this->ExecuteFunction(
			this->g_pScriptVM,
			hFunction,
			aArgs,
			sizeof...(args),
			nullptr,
			nullptr,
			true
		);
	}

private:
	bool hasToResetVM = true;
	void* g_pScriptVM = nullptr;
};

extern VScript* vscript;

#endif // VSCRIPT_HPP

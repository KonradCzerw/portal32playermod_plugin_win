#include "engine.hpp"

#include "offsets.hpp"
#include "vscript.hpp"

#include "console.hpp"

REDECL(Engine::LevelInit);

DETOUR_T(void, Engine::LevelInit, char* pMapName, char* pMapEntities, char* pOldLevel, char* pLandmarkName, bool loadGame, bool background) {
	vscript->ResetVM();
	Engine::LevelInit(thisptr, pMapName, pMapEntities, pOldLevel, pLandmarkName, loadGame, background);
}

Engine::Engine() {}

bool Engine::Init() {
	this->g_pServerPluginHandler = Interface::Create(this->Name(), "ISERVERPLUGINHELPERS001");
	if(this->g_pServerPluginHandler) {
		this->g_pServerPluginHandler->Hook(Engine::LevelInit_Hook, Engine::LevelInit, Offsets::LevelInit);
	}

	this->g_pVEngineServer = Interface::Create(this->Name(), "VEngineServer022");
	if(this->g_pVEngineServer) {
		this->GetClientSteamID = g_pVEngineServer->Original<_GetClientSteamID>(Offsets::GetClientSteamID);
	}

	return this->g_pServerPluginHandler && this->g_pVEngineServer;
}

void Engine::Shutdown() {
	this->g_pVEngineServer->Unhook(Offsets::GetClientSteamID);
	this->g_pServerPluginHandler->Unhook(Offsets::LevelInit);
	Interface::Delete(this->g_pVEngineServer);
	Interface::Delete(this->g_pServerPluginHandler);
}

SteamID* Engine::GetSteamID(const edict_t* pPlayerEdict, bool bRequireFullyAuthenticated) {
	return this->GetClientSteamID(this->g_pVEngineServer->ThisPtr(), pPlayerEdict, bRequireFullyAuthenticated);
}

Engine* engine;
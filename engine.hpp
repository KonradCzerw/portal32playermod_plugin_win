#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "interface.hpp"
#include "utils.hpp"

class Engine {
private:
	Interface* g_pServerPluginHandler;
	Interface* g_pVEngineServer;

	using _GetClientSteamID = SteamID*(__thiscall*)(void* thisptr, const edict_t* pPlayerEdict, bool bRequireFullyAuthenticated);
	_GetClientSteamID GetClientSteamID = nullptr;

public:
	Engine();
	virtual ~Engine() = default;

	bool Init();
	void Shutdown();
	const char* Name() { return MODULE("engine"); }

public:
	SteamID* GetSteamID(const edict_t* pPlayerEdict, bool bRequireFullyAuthenticated);

public:
	DECL_DETOUR_T(void, LevelInit, char* pMapName, char* pMapEntities, char* pOldLevel, char* pLandmarkName, bool loadGame, bool background);
};

extern Engine* engine;

#endif // ENGINE_HPP

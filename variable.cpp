#include "variable.hpp"

#include "offsets.hpp"

Variable::Variable() : ptr(nullptr) {}

Variable::Variable(const char* pName) : Variable() {
	this->ptr = reinterpret_cast<ConVar*>(tier1->FindCommandBase(tier1->g_pCVar->ThisPtr(), pName));
}

Variable::~Variable() {}

void Variable::SetValue(int value) {
	Memory::VMT<_InternalSetIntValue>(this->ptr, Offsets::InternalSetIntValue)(this->ptr, value);
}

void* Variable::ThisPtr() {
	return reinterpret_cast<void*>(this->ptr);
}
